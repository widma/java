import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Vidmantas Naravas
 *
 */
public class Dice2 extends Dice1 {

	private int input;
	private int[] arrayOfSums;

	// Constructor
	Dice2() {
		arrayOfSums = new int[13]; //initiating array with size 13, that is, hold values from 0-12;
		Arrays.fill(arrayOfSums, 0); // filling an array with 0 to start with.
	}

	/**
	 * Method to produce table of rolls from user input. I'm using scanner to
	 * get user input of how many rolls is required, then validating input,
	 * looping until input is reached and in each loop rolling two dices,
	 * summing them and adding to array of sums.
	 */
	public void table() {
		Scanner user = new Scanner(System.in);
		System.out.println("How many times would you like to roll 2 dices?: ");
		// checking if user entered only integers. Unlimited
		while (!user.hasNextInt()) {
			System.out.println("Please use only integers.");
			user.next();
		}
		input = user.nextInt();
		// loop until input reached
		for (int i = 0; i < input; i++) {
			int a = rollDice(); // roll each dice individually
			int b = rollDice();
			int sum = a + b; //add them
			arrayOfSums[sum]++; //add sum to an array
		}
		print(arrayOfSums); // call method to print array of sums.
	}

	/**
	 * Method to print elements from array in specific format.
	 * 
	 * @param array
	 */
	public void print(int[] array) {
		System.out.printf("%-10s%10s\n", "Roll Sum", "Frequency");
		for (int i = 1; i < array.length; i++) {
			System.out.printf("%4s%12s\n", i, array[i]);
		}

	}

}
