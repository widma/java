
/**
 * @author http://digest.jacobparr.com/
 * 
 *         The actual distance source code was taken from here, I take no credit
 *         for this. What I did, was only to modify it so its more readable for
 *         human.
 * 
 *         https://en.wikipedia.org/wiki/Levenshtein_distance
 * 
 *         I didn't implement this using recursion, reason: its amazingly
 *         inefficient, because it recomputes the Levenshtein distance of the
 *         same substrings many times.
 *         
 *         
 *	@@@@@ Compile this file first @@@@@
 */
public final class LevenshteinDistance {

	/**
	 * @param first
	 *            String
	 * @param second
	 *            String
	 * @return cost of the transformation of the two string
	 */
	public static int distance(String first, String second) {
		// modify strings to lower case
		first = first.toLowerCase();
		second = second.toLowerCase();
		// array of cost, the size of the array is the misspelled word length in our
		// situation
		int[] costs = new int[second.length() + 1];

		// nested for loops, basically iterates trough first word 
		// and second word and generates single INT value for a word of how many
		// transformations is needed(insert,delete,substitute) in regards to
		// misspelled word and returns cost of it as a single value. Later this will be
		// used to get words which require 0 or 1 transformation maximum, in other
		// words:
		// closest match.
		for (int j = 0; j < costs.length; j++)
			costs[j] = j;
		{
			for (int i = 1; i <= first.length(); i++) {
				costs[0] = i;
				int nw = i - 1;
				for (int j = 1; j <= second.length(); j++) {
					int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
							first.charAt(i - 1) == second.charAt(j - 1) ? nw : nw + 1);
					nw = costs[j];
					costs[j] = cj;
				}
			}
		}
		return costs[second.length()];
	}

}
