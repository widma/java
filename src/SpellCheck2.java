import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author Vidmantas Naravas
 * @version Created on 18 Nov 2015
 * 
 *          This is a SpellChecker class. With SWING user interface, select
 *          dictionary file, then choose file name in the console to be checked
 *          through. If errors are found, program will suggest some options for
 *          user, like: choose suggested word or do nothing, or add word to
 *          dictionary. After options were chosen and file has ended, program
 *          will ask to type name of corrected file for output. This version
 *          uses Levenshtein distance algorithm.
 *          
 * @@@@@ Compile this file second @@@@@
 */
public class SpellCheck2 {
	/*
	 * Java swing instance
	 */

	private JFileChooser fileChooser = new JFileChooser();
	private File selectedFile = null;

	/*
	 * ArrayLists data structures to hold different objects throughout program.
	 */

	/*
	 * Holds words from dictionary file for comparison of words
	 */
	private ArrayList<String> dictionary = new ArrayList<String>();

	/*
	 * array of integers to hold values got from LevenshteinDistance method
	 */
	List<Integer> dist = new ArrayList<Integer>();

	/*
	 * An array to hold results which matches given conditions i.e, word with
	 * same SoundEx code and similar length
	 */
	private List<String> resultList = new ArrayList<String>();

	/*
	 * An array to hold String ready to be used with FileWriter
	 */
	private ArrayList<String> outputList = new ArrayList<String>();
	private ArrayList<Character> out = new ArrayList<Character>();
	/*
	 * boolean "cont" is used to continue looping. boolean "error" is used to
	 * check whether misspelled words has been detected.
	 */
	private boolean cont = true;
	private boolean error = false;
	/*
	 * String "word" is used to keep hold of current word got from scan.hasNext
	 * method throughout program String "toAdd" is used to add current word to
	 * dictionary in one of the options.
	 */
	private String toAdd;
	private String val; // variable to keep track of current char
	// variable to be used for menu options plus Scanner object to get users
	// input for menu options
	private int optionChose;
	private Scanner options = new Scanner(System.in);
	// keep track of the end of the file, used with FileReader
	private char r;
	String word = null;
	String test = null;

	/**
	 * This is the main method, where it spell checks words against dictionary
	 * and if word is misspelled gives user options what to do next. It calls
	 * function to load dictionary, then asks user for input file via console,
	 * then it will scan trough file until misspelled word has been found,
	 * generates SoundEx code for it, looks trough ArrayList of Dictionary
	 * objects, matches closest ones and produces suggestions list. User then
	 * can choose what to do. If no errors were found, simply print that nor
	 * errors were found and asks for output file name.
	 * 
	 * @throws IOException
	 */

	public void spellCheck() throws IOException {
		// loads dictionary
		dictionaryFile();
		/*
		 * Scanner object. Asks user for input of the file.
		 */
		Scanner user = new Scanner(System.in);
		System.out.print("Please enter input file with extension? ");
		String fileName = user.next().trim();

		FileInputStream in = new FileInputStream(fileName);

		while (in.available() > 0 && cont) {

			r = (char) in.read(); // reading every character in the file.
			out.add(r); // adds character to an ArrayList

			String temp; // creating temporary variable to store String from out
							// ArrayList
			// reads characters until encounters white space or non letter
			// character
			if (Character.isWhitespace(r) || !Character.isLetter(r)) {
				val = String.valueOf(r); // converts char to string
				temp = ""; // empty string to be filled with
							// characters
				// if out array is not empty, loop trough characters array and
				// build string out of them
				if (!out.isEmpty()) {
					for (int i = 0; i < out.size() - 1; i++) {
						temp = temp + out.get(i);
					}

					test = temp;
					outputList.add(temp); // add builded string into output
											// array
				}
				outputList.add(val); // add non letter character to the same
										// output
				// assigning temp to variable word
				word = temp;

				word = word.toLowerCase(); // convert word to lowercase to
											// prevent
				// misspelling on correct words
				// starting to check the word against dictionary
				// this would translate as follows: if dictionary ArrayList
				// doesnt't contain word looked for and word looked for is not
				// empty, proceed
				if (!dictionary.contains(word) && !temp.isEmpty()) {
					error = true; // set error boolean to true
					// this is used if option add to dictionary was selected
					toAdd = word;
					// looping trough each word from dictionary and comparing it
					// to misspelled word by sending it to distance class and
					// getting int values from it, then adding it to Integer
					// list
					for (int i = 0; i < dictionary.size(); i++) {
						dist.add(LevenshteinDistance.distance(dictionary.get(i), word));
					}
					// sorting array in ascending order
					Collections.sort(dist);
					// looping trough dictionary, checking which words has 0
					// transformation value, which would imply, they are the
					// closest match, then adding those words to result list
					// ArrayList
					for (int i = 0; i < dictionary.size(); i++) {
						if (LevenshteinDistance.distance(dictionary.get(i), word) == dist.get(0)) {
							resultList.add(dictionary.get(i));
						}
					}

					System.out.println(); // print empty line ( more for
					// // esthetics)
					// // print misspelled word and text
					System.out.println("Looks like: " + "'" + word + "'" + ", is misspelled, here's suggestions: ");
					System.out.println();
					// max number of suggestions
					int maxSuggesions = 4;
					for (int i = 0; i < resultList.size(); i++) {
						System.out.println("(" + i + ")" + " " + resultList.get(i));
						if (i == maxSuggesions) {
							break;
						}
					}
					// stop looping
					cont = false;
					System.out.println();
					// print Suggestions and options
					System.out.println("---- Please choose a number for suggestions ");
					System.out.println("---- Or choose the following: ");
					System.out.println();
					System.out.println("(" + 5 + ")" + " Leave word as is.");
					System.out.println("(" + 6 + ")" + " Add word to dictionary.");
					System.out.println(
							"(" + 7 + ")" + " Type your own word, add to dictionary and keep it in output file.");

					// call function to validated user input and assign it
					// to
					// variable optionChose
					optionChose = getNumber(0, 7);

					// call function with passed parameter
					choose(optionChose);
				}

				resultList.clear();
				out.clear();
			}

		}

		// // closing FileReader
		in.close();
		// // if no error were found, print this message
		if (!error) {
			System.out.println("No errors were found.");
		}
		// call function to output file
		output();
	}

	/**
	 * This method add dictionary file to be used within program. It uses Swing
	 * user interface to pick file from user system.
	 * 
	 * @throws IOException
	 */
	public void dictionaryFile() throws IOException {
		String temp;
		// only allowing .dic or .txt file formats to be used
		fileChooser.setDialogTitle("Please select Dictionary file.");
		fileChooser.setAcceptAllFileFilterUsed(false);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Dictionary files: .dic or .txt", "dic", "txt");
		// adding filter into swing
		fileChooser.addChoosableFileFilter(filter);
		// setting which directory to open when program is called
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		// showing swing dialog screen
		int result = fileChooser.showOpenDialog(fileChooser);
		// if file was OK, print file destination
		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
			System.out.println("Selected dictionary file: " + selectedFile.getAbsolutePath());
		}
		// Initializing FileReader object with file got from swing file chooser
		FileReader fileReader = new FileReader(selectedFile);
		// wrapping it in BufferedReader
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		// scanning trough whole file and adding every word in the dictionary
		// and at the same time creating DictionaryObject with SoundEx code and
		// word with for every word found in he dictionary.
		while ((temp = bufferedReader.readLine()) != null) {
			dictionary.add(temp);
		}
		// always close BufferReader
		bufferedReader.close();

	}

	/**
	 * This method for menu logic. 0-4 cases gets item from resultList and adds
	 * suggestion words to the output List, switch cont to true and breaks from
	 * switch. Case 7 is a bit different, it will prompt for the user input to
	 * type new word, will add it to the dictionary and to the output file.
	 * 
	 * @param chosen
	 *            value for switch statement
	 * @return chosen case
	 * @throws IOException
	 */
	public boolean choose(int chosen) throws IOException {
		int itemIndex;
		switch (chosen) {
		case 0:
			String a = resultList.get(0);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, a);
			cont = true;
			break;
		case 1:
			String b = resultList.get(1);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, b);
			cont = true;
			break;
		case 2:
			String c = resultList.get(2);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, c);
			cont = true;
			break;
		case 3:
			String d = resultList.get(3);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, d);
			cont = true;
			break;
		case 4:
			String e = resultList.get(4);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, e);
			cont = true;
			break;
		case 5:
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, test);
			cont = true;
			break;
		case 6:
			itemIndex = outputList.indexOf(word);
			outputList.set(itemIndex, toAdd);
			addToDic(toAdd);
			cont = true;
			break;
		case 7:
			itemIndex = outputList.indexOf(word);
			String newWord = typeOwn();
			outputList.set(itemIndex, newWord);
			addToDic(newWord);
			cont = true;
			break;
		}
		return false;
	}

	/**
	 * This method uses BufferedWrited to append string to existing list. It
	 * gets file from swing selected file a	nd word from current string in "word"
	 * variable.
	 * @param tAddword 
	 * @throws IOException
	 */
	public void addToDic(String tAddword) throws IOException {
		BufferedWriter buffWriter = null;
		try {
			buffWriter = new BufferedWriter(new FileWriter(selectedFile, true));
			buffWriter.write(tAddword);
			buffWriter.newLine();
			buffWriter.flush();
		} catch (IOException inputOutputE) {
			inputOutputE.printStackTrace();
		} finally {
			System.out.println("Succesfully added word to " + selectedFile);
		}

	}

	/**
	 * This method writes to the file. Uses scanner to get user input for file
	 * name. Then uses FileWriter wrapped with BufferedWriter to write elements
	 * from array to the file.
	 * 
	 * @throws IOException
	 */
	public void output() throws IOException {
		Scanner out = new Scanner(System.in);
		System.out.println();
		System.out.print("Output file name? ");
		String outName = out.next().trim();
		FileWriter fileWriter = new FileWriter(outName);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		// this is a bridge between normal array and collections API and it
		// returns
		// all elements from a list(Collection) in proper sequence.
		String[] output = outputList.toArray(new String[0]);
		// for each to loop trough array and write each element in the file
		for (String element : output) {
			bufferedWriter.write(element);
		}
		// print message to user if file was successfully created
		System.out.println("File was succesfully created at: " + System.getProperty("user.dir"));
		bufferedWriter.close(); // close BufferedWriter
	}

	/**
	 * Method to validated users input. Prints message with valid options and
	 * checks if input was integer and within set boundaries.
	 * 
	 * @param lower
	 *            integer
	 * @param upper
	 *            integer
	 * @return number
	 */
	public int getNumber(int lower, int upper) {
		do {
			System.out.println();
			System.out.println("Please enter integer between " + lower + " and " + upper + ": ");
			while (!options.hasNextInt()) {
				System.out.println("Only integers is allowed, please be careful. Choose again: ");
				options.next();
			}
			optionChose = options.nextInt();
		} while ((optionChose < lower) || (optionChose > upper));
		return optionChose;
	}

	/**
	 * Method to get user input. It's used when number 7 is pressed from the
	 * menu. Basically takes whatever has been written in the console.
	 * 
	 * @return String from user input
	 */
	public String typeOwn() {
		Scanner own = new Scanner(System.in);
		System.out.println("--->Please type your word");
		String newWord = own.nextLine();
		return newWord;
	}

}
