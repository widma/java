
/**
 * @author Vidmantas Naravas
 * @version 1.0 Created 17 November 2015
 * 
 * 
 * With help of this class I can create an Object
 * with SoundEx code and associated word to it.
 * It creates an Object(0000,word) in this format.
 */
public class DictionaryObject {
	// Attributes
	private String soundCode;
	private String word;

	/*
	 * Constructor
	 */
	DictionaryObject(String soundCode, String word) {
		this.soundCode = soundCode;
		this.word = word;
	}

	/**
	 * @return SoundCode value
	 */
	public String getSoundCode() {
		return soundCode;
	}

	/**
	 * @return word
	 */
	public String getWord() {
		return word;
	}

	@Override
	public String toString() {
		return "Diction [soundCode=" + soundCode + ", word=" + word + "]";
	}

	/**
	 * Since there's a lot of words in the dictionary with same SoundEx code I'm
	 * trying to narrow findings only if word is 1 char shorter or 1 char longer
	 * or has same length
	 * 
	 * @param firstWord
	 * @param secondWord
	 * @return true or false
	 */
	public boolean checkLength(String firstWord, String secondWord) {
		if (firstWord.length() == secondWord.length() + 1 || firstWord.length() == secondWord.length() - 1
				|| firstWord.length() == secondWord.length()) {
			return true;
		}
		return false;
	}

}
