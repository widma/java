import java.io.IOException;

/**
 * @author Vidmantas Naravas
 *         <a href="mailto:ma301vn@gold.ac.uk>">ma301vn@gold.ac.uk</a>
 * @version 03 Dec 2015
 *
 * @@@@ Compile this file third @@@@
 * 			Then run it
 */ 

public class Main {

	/**
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {

		SpellCheck2 a = new SpellCheck2();
		try {
			a.spellCheck();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
