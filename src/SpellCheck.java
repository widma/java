import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;
import javax.swing.JFileChooser;

/**
 * @author Vidmantas Naravas
 * @version Created on 18 Nov 2015
 * 
 *          This is a SpellChecker class. With SWING user interface, select
 *          dictionary file, then choose file name in the console to be checked
 *          through. If errors are found, program will suggest some options for
 *          user, like: choose suggested word or do nothing, or add word to
 *          dictionary. After options were chosen and file has ended, program
 *          will ask to type name of corrected file for output.
 */
public class SpellCheck {
	/*
	 * Java swing instance.
	 */

	private JFileChooser fileChooser = new JFileChooser();
	private File selectedFile = null;

	/*
	 * ArrayLists data structures to hold different objects throughout program.
	 */

	/*
	 * Holds instances of DictionaryObject class
	 */
	private List<DictionaryObject> DicObjects = new ArrayList<DictionaryObject>();

	/*
	 * Holds words from dictionary file for comparison of words
	 */
	private ArrayList<String> dictionary = new ArrayList<String>();

	/*
	 * An array to hold results which matches given conditions i.e, word with
	 * same SoundEx code and similar length
	 */
	private List<String> resultList = new ArrayList<String>();

	/*
	 * An array to hold String ready to be used with FileWriter
	 */
	private ArrayList<String> outputList = new ArrayList<String>();
	private ArrayList<Character> out = new ArrayList<Character>();
	/*
	 * boolean "cont" is used to continue looping. boolean "error" is used to
	 * check whether misspelled words has been detected.
	 */
	private boolean cont = true;
	private boolean error = false;
	/*
	 * String "word" is used to keep hold of current word got from scan.hasNext
	 * method throughout program String "toAdd" is used to add current word to
	 * dictionary in one of the options.
	 */
	private String temp = null;
	private String toAdd;
	private String val; // variable to keep track of current char
	// variable to be used for menu options plus Scanner object to get users
	// input for menu options
	private int optionChose;
	private Scanner options = new Scanner(System.in);
	// keep track of the end of the file, used with FileReader
	private char r;
	String word = null;
	String test = null;
	/**
	 * This is the main method, where it spell checks words against dictionary
	 * and if word is misspelled gives user options what to do next. It calls
	 * function to load dictionary, then asks user for input file via console,
	 * then it will scan trough file until misspelled word has been found,
	 * generates SoundEx code for it, looks trough ArrayList of Dictionary
	 * objects, matches closest ones and produces suggestions list. User then
	 * can choose what to do. If no errors were found, simply print that nor
	 * errors were found and asks for output file name.
	 * 
	 * @throws IOException
	 */

	public void spellCheck() throws IOException {
		// loads dictionary
		dictionaryFile();
		/*
		 * Scanner object. Asks user for input of the file.
		 */
		Scanner user = new Scanner(System.in);
		System.out.print("Please enter input file with extension? ");
		String fileName = user.next().trim();

		FileInputStream in = new FileInputStream(fileName);

		while (in.available() > 0 && cont) {

			r = (char) in.read(); // reading every character in the file.
			out.add(r); // adds character to an ArrayList

			String temp; // creating temporary variable to store String from out
							// ArrayList
			// reads characters until encounters white space or non letter
			// character
			if (Character.isWhitespace(r) || !Character.isLetter(r)) {
				val = String.valueOf(r); // converts char to string
				temp = ""; // empty string to be filled with
							// characters
				// if out array is not empty, loop trough characters array and
				// build string out of them
				if (!out.isEmpty()) {
					for (int i = 0; i < out.size() - 1; i++) {
						temp = temp + out.get(i);
					}

					test = temp;
					outputList.add(temp); // add builded string into output
											// array
				}
				outputList.add(val); // add non letter character to the same
										// output
				// assigning temp to variable word
				word = temp;
				
				word = word.toLowerCase(); // convert word to lowercase to prevent
									// misspelling on correct words
				// starting to check the word against dictionary
				// this would translate as follows: if dictionary ArrayList
				// doesnt't contain word looked for and word looked for is not
				// empty, proceed
				if (!dictionary.contains(word) && !temp.isEmpty()) {
					error = true; // set error boolean to true
					// this is used if option add to dictionary was selected
					toAdd = word;
					// get SoundEx code for that word
					String code = Soundex.getGode(word);
					System.out.println(); // print empty line ( more for
											// esthetics)
					// print misspelled word and text
					System.out.println("Looks like: " + "'" + word + "'" + ", is misspelled, here's suggestions: ");
					// for each loop was used to loop through dictionary
					// objects
					for (DictionaryObject items : DicObjects) {
						// only select those words where comply with
						// conditions,
						// that is: get soundCode, which matches and check
						// word
						// length against word lengths in dictionary objects
						if (items.getSoundCode().equalsIgnoreCase(code) && items.checkLength(word, items.getWord())) {
							// if found, append those word to an ArrayList
							resultList.add(items.getWord());
						}
					}
					// Print suggestions, in my case I've chosen 5. SoundEx
					// is
					// not
					// particularly good with dictionaries, because out of
					// the
					// hundreds of words with same SoundEx code even with my
					// constrains, I still getting too many suggestions
					int maxSuggesions = 5;
					for (int i = 0; i < resultList.size(); i++) {
						System.out.println("(" + i + ")" + " " + resultList.get(i));
						if (i == maxSuggesions) {
							break;
						}
					}
					// stop looping
					cont = false;
					// print Suggestions and options
					System.out.println("---- Please choose a number for what to do next? ");
					System.out.println("---- Or choose the following: ");
					System.out.println("(" + 5 + ")" + " Leave word as is.");
					System.out.println("(" + 6 + ")" + " Add word to dictionary.");

					// call function to validated user input and assign it
					// to
					// variable optionChose
					optionChose = getNumber(0, 6);

					// call function with passed parameter
					choose(optionChose);
				}

				resultList.clear();
				out.clear();
			}

		}

		// // closing FileReader
		in.close();
		// // if no error were found, print this message
		if (!error) {
			System.out.println("No errors were found.");
		}
		// call function to output file
		output();
	}

	/**
	 * This method add dictionary file to be used within program. It uses Swing
	 * user interface to pick file from user system.
	 * 
	 * @throws IOException
	 */
	public void dictionaryFile() throws IOException {
		// setting which directory to open when program is called
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		// showing swing dialog screen
		int result = fileChooser.showOpenDialog(fileChooser);
		// if file was OK, print file destination
		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
			System.out.println("Selected dictionary file: " + selectedFile.getAbsolutePath());
		}
		// Initializing FileReader object with file got from swing file chooser
		FileReader fileReader = new FileReader(selectedFile);
		// wrapping it in BufferedReader
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		// scanning trough whole file and adding every word in the dictionary
		// and at the same time creating DictionaryObject with SoundEx code and
		// word with for every word found in he dictionary.
		while ((temp = bufferedReader.readLine()) != null) {
			dictionary.add(temp);
			DicObjects.add(new DictionaryObject(Soundex.getGode(temp), temp));
		}
		// always close BufferReader
		bufferedReader.close();

	}

	/**
	 * This method for menu logic. 0-4 cases gets item from resultList and adds
	 * suggestion word to output List, switch cont to true and breaks from
	 * switch.
	 * 
	 * @param chosen
	 *            value for switch statement
	 * @return chosen case
	 * @throws IOException
	 */
	public boolean choose(int chosen) throws IOException {
		int itemIndex;
		switch (chosen) {
		case 0:
			String a = resultList.get(0);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, a);
			cont = true;
			break;
		case 1:
			String b = resultList.get(1);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, b);
			cont = true;
			break;
		case 2:
			String c = resultList.get(2);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, c);
			cont = true;
			break;
		case 3:
			String d = resultList.get(3);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, d);
			cont = true;
			break;
		case 4:
			String e = resultList.get(4);
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, e);
			cont = true;
			break;
		case 5:
			itemIndex = outputList.indexOf(test);
			outputList.set(itemIndex, test);
			cont = true;
			break;
		case 6:
			itemIndex = outputList.indexOf(word);
			outputList.set(itemIndex, toAdd);
			addToDic(toAdd);
			cont = true;
		}
		return false;
	}

	/**
	 * This method uses BufferedWrited to append string to existing list. It
	 * gets file from swing selected file and word from current string in "word"
	 * variable.
	 * 
	 * @param word
	 * @throws IOException
	 */
	public void addToDic(String word) throws IOException {
		BufferedWriter buffWriter = null;
		try {
			buffWriter = new BufferedWriter(new FileWriter(selectedFile, true));
			buffWriter.write(toAdd);
			buffWriter.newLine();
			buffWriter.flush();
		} catch (IOException inputOutputE) {
			inputOutputE.printStackTrace();
		} finally {
			System.out.println("Succesfully added word to " + selectedFile);
		}

	}

	/**
	 * This method writes to the file. Uses scanner to get user input for file
	 * name. Then uses FileWriter wrapped with BufferedWriter to write elements
	 * from array to the file.
	 * 
	 * @throws IOException
	 */
	public void output() throws IOException {
		Scanner out = new Scanner(System.in);
		System.out.println();
		System.out.print("Output file name? ");
		String outName = out.next().trim();
		FileWriter fileWriter = new FileWriter(outName);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		// this is a bridge between normal array and collections API and it
		// returns
		// all elements from a list(Collection) in proper sequence.
		String[] output = outputList.toArray(new String[0]);
		// for each to loop trough array and write each element in the file
		for (String element : output) {
			bufferedWriter.write(element);
		}
		// print message to user if file was successfully created
		System.out.println("File was succesfully created at: " + System.getProperty("user.dir"));
		bufferedWriter.close(); // close BufferedWriter
	}

	/**
	 * Method to validated users input. Prints message with valid options and
	 * checks if input was integer and within set boundaries. Surrounded with
	 * try/catch statements to handle exceptions.
	 * 
	 * @param lower
	 *            integer
	 * @param upper
	 *            integer
	 * @return number
	 */
	public int getNumber(int lower, int upper) {
		do {
			try {

				System.out.println("--------------");
				System.out.println("Please enter integer  between " + lower + " and " + upper + ": ");
				if ((optionChose < lower) || (optionChose > upper)) {
					System.out.println("Input is not between " + lower + " and " + upper + " and try again.");
				}
				optionChose = options.nextInt();
			} catch (Exception e) {
				System.out.println("Input is not an integer, try again.");
				options.next();

			}
		} while ((optionChose < lower) || (optionChose > upper));

		return optionChose;

	}
}
