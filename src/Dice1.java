import java.util.*;

/**
 * Class to roll 6 sided dice.
 * 
 * @author Vidmantas Naravas
 *
 */
public class Dice1 {
	// Attributes
	private int low = 1;
	private int high = 7;

	// default constructor
	Dice1() {
	}

	/**
	 * Returns random number between 1 to 6
	 * 
	 * @return value
	 */
	public int rollDice() {
		Random randomValue = new Random();
		int value = randomValue.nextInt(high - low) + low;
		return value;
	}
}
