
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.*;

/**
 * @author Vidmantas Naravas
 *
 */
public class UserInterface extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JButton addButton = new JButton("Add file");
	private JButton dicButton = new JButton("Add dictionary");
	private JButton startC = new JButton("Start Checking");
	private JButton option1 = new JButton("Choose1");
	private JButton option2 = new JButton("Choose2");
	private JButton option3 = new JButton("Choose3");
	private JButton option4 = new JButton("Choose4");
	private JButton option5 = new JButton("Choose5");
	private JButton option6 = new JButton("Leave");
	private JButton option7 = new JButton("Add to dic");
	private JButton save = new JButton("Save file..");
	private JTextArea textArea = new JTextArea(8, 45);
	private JTextArea outArea = new JTextArea(8, 45);
	/*
	 * 
	 * 
	 */
	private ArrayList<String> text = new ArrayList<String>();
	private ArrayList<String> dictionary = new ArrayList<String>();
	private ArrayList<String> outputList = new ArrayList<String>();
	private ArrayList<Character> out = new ArrayList<Character>();
	List<Integer> dist = new ArrayList<Integer>();
	private List<String> resultList = new ArrayList<String>();
	private boolean cont = false;
	private boolean error = false;
	/*
	 * String "word" is used to keep hold of current word got from scan.hasNext
	 * method throughout program String "toAdd" is used to add current word to
	 * dictionary in one of the options.
	 */
	private String temp = null;
	private String toAdd;
	private String val; // variable to keep track of current char
	// variable to be used for menu options plus Scanner object to get users
	// input for menu options
	private int optionChose;
	private Scanner options = new Scanner(System.in);
	// keep track of the end of the file, used with FileReader
	private char r;
	String word = null;
	String test = null;
	JFileChooser fileChooser = new JFileChooser();
	File selectedFile;
	String a = new String("Please add dictionary first and then add file to be checked.");

	public UserInterface() {

		// layout
		setLayout(new FlowLayout());
		// default action when press cross
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// appearance
		setTitle("Spell Checker");
		setSize(600, 400);
		setLocation(400, 100);
		setResizable(false);
		// pack();
		// setLocationRelativeTo(null);
		// getContentPane().setBackground(Color.cyan);
		// borders

		// button add
		add(addButton);
		addButton.setBackground(Color.darkGray);

		add(startC);
		startC.setBackground(Color.darkGray);

		add(dicButton);
		dicButton.setBackground(Color.darkGray);

		add(save);
		save.setBackground(Color.darkGray);

		// text area 1
		textArea.setBorder(
				new TitledBorder(new LineBorder(Color.black), "Text", TitledBorder.CENTER, TitledBorder.TOP));
		JScrollPane panel1 = new JScrollPane(textArea);
		// textArea.add(panel1, BorderLayout.CENTER);

		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);

		add(panel1);
		// out area
		outArea.setBorder(new TitledBorder(new LineBorder(Color.black), "Info", TitledBorder.CENTER, TitledBorder.TOP));
		JScrollPane panel2 = new JScrollPane(outArea);
		outArea.setEditable(false);
		add(panel2);

		addButton.addActionListener(this);
		dicButton.addActionListener(this);
		startC.addActionListener(this);
		save.addActionListener(this);
		textArea.setText(a);

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addButton) {
			try {
				file();

			} catch (Exception a) {

				a.getMessage();
			}
		}
		if (e.getSource() == startC) {

			// spell(selectedFile);
			try {
				cont = true;

				spell(selectedFile);

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.getMessage();
			}

			if (e.getSource() == save) {

			}
			if (e.getSource() == dicButton) {
				try {
					dictionaryFile();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		}
	}

	/**
	 * 
	 */
	public void file() {
		String temp;

		FileReader fileReader = null;
		// setting which directory to open when program is called
		fileChooser.setCurrentDirectory(
				new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Documents"));
		// showing swing dialog screen
		int result = fileChooser.showOpenDialog(fileChooser);
		// if file was OK, print file destination
		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
			// System.out.println("Selected dictionary file: " +
			// selectedFile.getAbsolutePath());
		}
		// Initializing FileReader object with file got from swing file
		// chooser
		try {
			fileReader = new FileReader(selectedFile);

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// wrapping it in BufferedReader
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		// scanning trough whole file and adding every word in the
		// dictionary
		// and at the same time creating DictionaryObject with SoundEx code
		// and
		// word with for every word found in he dictionary.
		try {
			while ((temp = bufferedReader.readLine()) != null) {
				text.add(temp);
				// DicObjects.add(new
				// DictionaryObject(Soundex.getGode(temp),
				// temp));
			}

			for (int i = 0; i < text.size(); i++) {
				textArea.setText(text.get(i));
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// always close BufferReader
		try {
			bufferedReader.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void dictionaryFile() throws IOException {
		String temp;
		JFileChooser filea = new JFileChooser();
		File selectedFile = null;
		// FileReader fileReader = null;
		// setting which directory to open when program is called
		filea.setCurrentDirectory(new File(System.getProperty("user.home")));
		// showing swing dialog screen
		int result = filea.showOpenDialog(filea);
		// if file was OK, print file destination
		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFile = filea.getSelectedFile();
			System.out.println("Selected dictionary file: " + selectedFile.getAbsolutePath());
		}
		// Initializing FileReader object with file got from swing file chooser
		FileReader fileReader = new FileReader(selectedFile);
		// wrapping it in BufferedReader
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		// scanning trough whole file and adding every word in the dictionary
		// and at the same time creating DictionaryObject with SoundEx code and
		// word with for every word found in he dictionary.
		while ((temp = bufferedReader.readLine()) != null) {
			dictionary.add(temp);
			// DicObjects.add(new DictionaryObject(Soundex.getGode(temp),
			// temp));
		}
		// always close BufferReader
		bufferedReader.close();

	}

	public void spell(File selectedFile) throws IOException {

		FileInputStream in = new FileInputStream(selectedFile);

		while (in.available() > 0 && cont) {

			r = (char) in.read(); // reading every character in the file.
			out.add(r); // adds character to an ArrayList

			String temp; // creating temporary variable to store String from out
							// ArrayList
			// reads characters until encounters white space or non letter
			// character
			if (Character.isWhitespace(r) || !Character.isLetter(r)) {
				val = String.valueOf(r); // converts char to string
				temp = ""; // empty string to be filled with
							// characters
				// if out array is not empty, loop trough characters array and
				// build string out of them
				if (!out.isEmpty()) {
					for (int i = 0; i < out.size() - 1; i++) {
						temp = temp + out.get(i);
					}

					test = temp;
					outputList.add(temp); // add builded string into output
											// array
				}
				outputList.add(val); // add non letter character to the same
										// output
				// assigning temp to variable word
				word = temp;

				word = word.toLowerCase(); // convert word to lowercase to
				// prevent
				// misspelling on correct words
				// starting to check the word against dictionary
				// this would translate as follows: if dictionary ArrayList
				// doesnt't contain word looked for and word looked for is not
				// empty, proceed
				if (!dictionary.contains(word) && !temp.isEmpty()) {
					error = true; // set error boolean to true
					// this is used if option add to dictionary was selected
					toAdd = word;

					for (int i = 0; i < dictionary.size(); i++) {
						dist.add(LevenshteinDistance.distance(dictionary.get(i), word));
					}
					Collections.sort(dist);
					for (int i = 0; i < dictionary.size(); i++) {
						if (LevenshteinDistance.distance(dictionary.get(i), word) == dist.get(0)) {
							// System.out.print(dictionary.get(i) + " ");
							resultList.add(dictionary.get(i));
						}
					}

					// get SoundEx code for that word
					// String code = Soundex.getGode(word);
					// System.out.println(); // print empty line ( more for
					// // esthetics)
					// // print misspelled word and text
					// System.out.println("Looks like: " + "'" + word + "'" + ",
					// is misspelled, here's suggestions: ");
					// System.out.println();
					outArea.append("Looks like: " + "'" + word + "'" + ", is misspelled, here's suggestions: ");
					// // for each loop was used to loop through dictionary
					// // objects
					// for (DictionaryObject items : DicObjects) {
					// // only select those words where comply with
					// // conditions,
					// // that is: get soundCode, which matches and check
					// // word
					// // length against word lengths in dictionary objects
					// if (items.getSoundCode().equalsIgnoreCase(code) &&
					// items.checkLength(word, items.getWord())) {
					// // if found, append those word to an ArrayList
					// resultList.add(items.getWord());
					// }
					// }
					// Print suggestions, in my case I've chosen 5. SoundEx
					// is
					// not
					// particularly good with dictionaries, because out of
					// the
					// hundreds of words with same SoundEx code even with my
					// constrains, I still getting too many suggestions
					int maxSuggesions = 5;
					for (int i = 0; i < resultList.size(); i++) {
						// System.out.println("(" + i + ")" + " " +
						// resultList.get(i));
						outArea.append("(" + i + ")" + " " + resultList.get(i));
						if (i == maxSuggesions) {
							break;
						}
					}
					// stop looping
					cont = false;
					// print Suggestions and options
					System.out.println("---- Please choose a number for what to do next? ");
					System.out.println("---- Or choose the following: ");
					System.out.println("(" + 5 + ")" + " Leave word as is.");
					System.out.println("(" + 6 + ")" + " Add word to dictionary.");

					// call function to validated user input and assign it
					// to
					// variable optionChose
					// optionChose = getNumber(0, 6);

					// call function with passed parameter
					// choose(optionChose);
				}

				resultList.clear();
				out.clear();
			}

		}

		// // closing FileReader
		// in.close();
		// // if no error were found, print this message
		if (!error) {
			System.out.println("No errors were found.");
		}
		// call function to output file
		// output();
	}
}
